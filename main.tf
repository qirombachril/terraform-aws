provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/18"

  tags = {
    Name = "Main VPC"
  }
}

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "Public Subnet"
  }
}

resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Private Subnet"
  }
}

resource "aws_nat_gateway" "private_nat" {
  connectivity_type = "private"
  subnet_id         = aws_subnet.private.id
  tags = {
    Name = "Main NAT Gateway"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_launch_configuration" "launch" {
  name_prefix   = "terraform-lc-"
  image_id      = data.aws_ami.ubuntu.id
  instance_type = "t2.medium"

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_group" "asg_conf" {
  name                 = "terraform-asg"
  launch_configuration = aws_launch_configuration.launch.name
  min_size             = 2
  max_size             = 5
  vpc_zone_identifier  = [aws_subnet.private.id]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "asp_conf" {
  name                   = "terraform-autoscaling_policy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.asg_conf.name
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_metric_alarm_conf" {
  alarm_name          = "terraform-cloudwatch_metric_alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "45"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.asg_conf.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.asp_conf.arn]
}
